---
title: "RMBL Bees"
output:
  word_document: default
  pdf_document: default
  html_notebook: default
---
##Code by Laura Naslund, who can be reached for questions at lauranaslund@gmail.com

#In this code chunk, the packages and csvs are loaded
```{r load, clean, summary stats, include=FALSE}
#packages
library(tidyverse)
library(magrittr)
library(lubridate)
library(MASS)
library(lme4)
library(knitr)
library(lmerTest)
library(DescTools)
#library("bibtex")

#citations
#write.bib("lme4", file = "lme4.bib")
#write.bib("lmerTest", file="lmerTest.bib")

#load csvs
#load data long-term bee data, raw dataset named "bees_all"
bees_all <- read.csv("bees_2018-06-13.csv", stringsAsFactors=FALSE)
#load climate data from billy barr
climate <- read.csv("billy_weather_2.csv")
#load thermal data
thermal <- read.csv("thermal_data_final.csv")
#load site data
site_info <- read.csv("predict_site_info.csv")
#load traits dataset
traits <- read.csv("traits_by_species.csv")
#load sampling effort
effort <- read.csv("sampling_effort.csv")

``` 

#In this code chunk, the long-term data contained in a df called "bees_all" is cleaned, creating a new df called "bees_clean". Cleaning involved the removal of non-bees, removal of decomissioned and mispelled sites, and converting the catch method column to a standard format.
```{r clean bees}
#determine number of total observations=42827
#nrow(bees_all)

#variable names
#names(bees_all)

#clean data
##genera to remove
remove_genera <- c("", "wasp", "Wasp", "Fly", "Belomicrus", "Crabronina", "Nysson", "Pemphredon", "Pemphredonina", "Pison", "Psenini", "Tachysphex")

##sites to keep
keep_site <- c("Almont", "Almont curve", "Almont Curve", "Beaver", "CDOT", "Copper", "Davids", "Elko", "Gothic", "Hill", "Kettle ponds", "Kettle Ponds", "Little", "Lypps", "Mexican Cut", "Rustlers", "Seans", "Snodgrass", "snodgrass", "Tuttle", "Willey")

#filter, cleaned dataset named "bees_clean"
##nrow 35115
bees_clean <- bees_all %>% filter(!(genus %in% remove_genera)) %>% filter(site %in% keep_site)

#wrong year
bees_clean$year[17493] <- 2014

#change method to be consistent
levels(as.factor(bees_clean$method))
bowl_descr <- c("bowl", "Bowl")
net_descr <- c("Net", "Net, Am", "Net, AM", "Net, PM")

for(i in 1:nrow(bees_clean)){
  if(bees_clean$method[i] %in% bowl_descr){
    bees_clean$method_clean[i] <- "bowl"
  }
  if(bees_clean$method[i] %in% net_descr){
    bees_clean$method_clean[i] <- "net"
  }
}

#make method a factor so you can group by it
bees_clean$method_clean <- as.factor(bees_clean$method_clean)
```

#In this code chunk, a new date column is added that combines the month, day and year and converts in to the lubridate Date class
```{r format date, include=FALSE}
##add date to bees_clean
#create new column sample_day
#create a copy of the date_sampled so it isn't lost when it is split
bees_clean <- bees_clean %>% mutate(date_sampled_copy = date_sampled) %>% separate(date_sampled, into = c("d", "m"), sep = "-") %>% mutate(month_num=match(m, month.abb)) %>% mutate(sample_day = paste(month_num, d, year, sep="-")) 

#make date class
bees_clean$sample_day <- mdy(bees_clean$sample_day)

#get rid of added columns
bees_clean <- bees_clean %>% dplyr::select(-d, -m, -month_num)
```

#In this code chunk, a combined date column is added to the sampling effort df called "effort"
```{r effort}
#add date to effort
effort <- effort %>% mutate(date_sampled_copy = date_sampled) %>% separate(date_sampled, into = c("d", "m"), sep = "-") %>% mutate(month_num=match(m, month.abb)) %>% mutate(sample_day = paste(year,month_num, d, sep="-")) 
```

#In this code chunk, the CTmax data contained in the df "thermal" is cleaned, creating a df called "thermal_clean". Cleaning involves the removal of nonbees and social bees and combining the genus and species column to create a "distinct_taxa" column. This chunk also prints a histogram of the CTmax as well as a table with the mean +- SD for mass and CTmax 
```{r thermal}
#number of bees=283
nrow(thermal %>% filter(!(genus%in% c("Wasp", "Fly"))))

#thermal_clean removes wasps and flies and bumble bees (not solitary)
thermal_clean <- thermal %>% filter(!(genus %in% c("Wasp", "Fly", "Bombus")))
thermal_clean <- droplevels(thermal_clean)

#range of thermal tolerance
range(thermal_clean$ctmax_thermo)

#histogram of thermal tolerance
ggplot(thermal_clean, aes(ctmax_thermo)) + 
  geom_histogram(binwidth=2) + 
  theme_classic() + 
  labs(x="CTmax", y="Count") +
  theme(axis.text=element_text(size=16, color="black", family="serif"), axis.title=element_text(size=20, family="serif"))

#shapiro-wilks test p=4.281e-08 not normal
shapiro.test(thermal_clean$ctmax_thermo)

#number of genera=15
levels(thermal_clean$genus)

#add a distinct taxa column combining genus and =22
for(i in 1:nrow(thermal_clean)){
  if (thermal_clean$species[i]=="") {
    thermal_clean$distinct_taxa[i] <- as.character(thermal_clean$genus[i])
    thermal_clean$genus_species[i] <- as.character(thermal_clean$genus[i])
  }
  else{
    thermal_clean$distinct_taxa[i] <- paste(thermal_clean$genus[i], thermal_clean$species[i], sep="_")
    thermal_clean$genus_species[i] <-paste(thermal_clean$genus[i], thermal_clean$species[i], sep=" ")
  }
}

#make distinct taxa a factor so you can group by it in plots
thermal_clean$distinct_taxa <- as.factor(thermal_clean$distinct_taxa)

#print the distinct taxa
levels(thermal_clean$distinct_taxa)

#mean + SD
thermal_clean %>% group_by(distinct_taxa, sex) %>% summarize(count= n(), mass_taxa_mg= mean(mass*1000), mass_sd= sd(mass*1000), mean_taxa = mean(ctmax_thermo), sd_taxa=sd(ctmax_thermo)) %>% arrange(desc(mean_taxa))

#table with summary of ctmax and mass
#write.csv(thermal_clean %>% group_by(distinct_taxa, sex) %>% summarize(count= n(), mass_taxa_mg= mean(mass*1000), mass_sd= sd(mass*1000), mean_taxa = mean(ctmax_thermo), sd_taxa=sd(ctmax_thermo)), "summary_ctmax_2.csv")

kruskal.test(ctmax_thermo ~distinct_taxa, data=thermal_clean)
#TukeyHSD(aov(taxa_lm), "distinct_taxa", data= thermal_clean)
```

#This code chunk prints a boxplot of the CTmax by distinct taxa
```{r large boxplot}
#large boxplot of distinct taxa
tiff('species_boxplot.tiff', units="in", width=16, heigh=9, res=300)
ggplot(thermal_clean, aes(x = reorder(distinct_taxa, ctmax_thermo, FUN = median), ctmax_thermo)) + 
  geom_boxplot()+ 
  geom_point() + 
  theme_bw() +
  labs(x="Taxa", y="CTmax") +
  theme(axis.title=element_text(size=20), axis.text.x = element_text(color="black", size=16, angle = 65, hjust = 1), axis.text.y=element_text(size=16, color="black"))+ 
  scale_x_discrete(labels=c("Andrena" = "Andrena spp.", "Colletes" = "Colletes spp.", "Dufourea_harveyi" = "Dufourea harveyi", "Habropoda"= "Habropoda sp.", "Halictus_rubicundus" = "Halictus rubicundus", "Halictus_virgatellus"= "Halictus virgatellus", "Hoplitis_robusta"= "Hoplitis robusta" , "Hylaeus" = "Hylaeus spp.", "Lasioglossum"= "Lasioglossum male", "Lasioglossum_inconditum"= "Lasioglossum inconditum", "Lasioglossum_nigrum"= "Lasioglossum nigrum", "Lasioglossum_ruidosense"= "Lasioglossum ruidosense", "Lasioglossum_tenax"="Lasioglossum tenax", "Megachile"="Megachile spp.", "Osmia"= "Osmia spp.", "Panurginus_cressoniellus"= "Panurginus cressoniellus", "Panurginus_ineptus"= "Panurginus ineptus", "Pseudopanurgus_bakeri"= "Pseduopanurgus bakeri", "Sphecodes"= "Sphecodes sp.", "Stelis"="Stelis sp."))
dev.off()

```

#This code chunk adds elevation to site information in the "thermal_clean" df and creates a table describing the number of NAs by site for each taxa to help determine which taxa have sufficient representation across elevations to analyze for elevational differences in CTmax by taxa
```{r adding elevation}
#check elevation 
check_elev <- site_info %>% dplyr::select(name, year, elevation_m)
check_elev <- spread(check_elev, year, elevation_m)

#elevation data
elev <- check_elev %>% dplyr::select("name", "2008")
names(elev) <- c("site", "elevation")

elev %>% filter(site %in% c("Beaver", "Davids", "Gothic", "Hill", "Little", "Rustlers", "Seans", "Tuttle", "Willey")) %>% arrange(elevation)

#add elevation to thermal_clean
thermal_clean <- left_join(thermal_clean, elev, by="site")

#add elevation for Mud site
thermal_clean$elevation[is.na(thermal_clean$elevation) == TRUE] <- 3047

#reorder the site by elevation
thermal_clean <- thermal_clean %>% mutate(site_elev = paste(site, elevation, sep="_"))

#table with number by group and site
site_summary <- spread((thermal_clean %>% group_by(distinct_taxa, site_elev) %>% summarize(n())), site_elev, "n()")

#create a table that lists number of NAs
#order by elevation
site_summary <- site_summary[,c(1, 9, 10, 2, 8, 7, 3, 6, 5, 4)]
#print
site_summary
#print as a count
site_summary %>% summarise_all(funs(sum(is.na(.)))) %>% mutate(sumVar = rowSums(.[2:10]))%>% arrange(sumVar)

#summary of number of each taxa captured
thermal_clean %>% group_by(distinct_taxa) %>% summarize(count=n()) %>% arrange(desc(count))

#separated by site
thermal_clean %>% group_by(distinct_taxa, site) %>% summarize(count=n()) %>% spread(site, count)
```

#This code chunk runs a mixed model for each taxa with sex, mass, it_span and elevation as fixed effects and site as a random effect
```{r elevation}
#sex mass itlength elevation (1|site)
#Andrena
require(lmerTest)
andrena.mixed <- lmer(ctmax_thermo~elevation + mass + sex + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Andrena"))
summary(andrena.mixed)

#Colletes
colletes.mixed <- lmer(ctmax_thermo~elevation + mass + sex + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Colletes"))
summary(colletes.mixed)

#Durfourea harveyi
dufoureaharveyi.mixed <- lmer(ctmax_thermo~elevation + mass + sex + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Dufourea_harveyi"))
summary(dufoureaharveyi.mixed)

#Hylaeus
#elevation is a significant predictor
hylaeus.mixed <- lmer(ctmax_thermo~elevation + mass + sex + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Hylaeus"))
summary(hylaeus.mixed)

#Lasioglossum
lasioglossum.mixed <- lmer(ctmax_thermo~elevation + mass +sex + it_length + (1|site), data=thermal_clean %>% filter(genus=="Lasioglossum"))
summary(lasioglossum.mixed)


#Lasioglossum M
lasioglossum.m.mixed <- lmer(ctmax_thermo~elevation + mass + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Lasioglossum"))
summary(lasioglossum.m.mixed)

#Lasioglossum F
lasioglossum.f.mixed <- lmer(ctmax_thermo~elevation + mass + it_length + (1|site), data=thermal_clean %>% filter(genus=="Lasioglossum", sex=="female"))
summary(lasioglossum.f.mixed)

#Megachile
#signficant
megachile.mixed <- lmer(ctmax_thermo~elevation +sex + mass + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Megachile"))
summary(megachile.mixed)

#Osmia
osmia.mixed <- lmer(ctmax_thermo~elevation + mass + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Osmia"))
summary(osmia.mixed)

#Panurginus cressoniellus
panurginusc.mixed <- lmer(ctmax_thermo~elevation +sex + mass + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Panurginus_cressoniellus"))
summary(panurginusc.mixed)

#Panurgins ineptus
panurginusi.mixed <- lmer(ctmax_thermo~elevation +sex + mass + it_length + (1|site), data=thermal_clean %>% filter(distinct_taxa=="Panurginus_ineptus"))
summary(panurginusi.mixed)
```

#This code chunk adds traits to thermal data set for resolved species creating df called "thermal_traits" which includes dummy variables for the traits
```{r traits set up (only includes ID to species so it could be paired with traits dataset)}
#rename traits columns
names(traits) <- c("distinct_taxa", "taxonomy", "phenology", "nest_loc", "nest_sub", "excav_occup", "ITD", "speciality", "sociality")

#merge traits and thermal dfs
thermal_traits <- inner_join(thermal_clean, traits, by="distinct_taxa")
thermal_traits <- droplevels(thermal_traits)

#make taxa a factor
thermal_traits$distinct_taxa <- as.factor(thermal_traits$distinct_taxa)

#to dummy variables nest_loc, nest_sub, excav_occup, speciality
#create dummy_sex 0=female, 1=male
for(i in 1:nrow(thermal_traits)){
  if(thermal_traits$sex[i]=="male"){
    thermal_traits$dummy_sex[i] <- 1
  }
  else{
    thermal_traits$dummy_sex[i] <- 0
  }
}

#create nest_loc dummy variable 0=below, 1=above
for(i in 1:nrow(thermal_traits)){
  if(thermal_traits$nest_loc[i]=="above"){
    thermal_traits$dummy_nest_loc[i] <- 1
  }
  else{
    thermal_traits$dummy_nest_loc[i] <- 0
  }
}

#create dummy_nest_sub, soil=0, stems=1
for(i in 1:nrow(thermal_traits)){
  if(thermal_traits$nest_sub[i]=="stems"){
    thermal_traits$dummy_nest_sub[i] <- 1
  }
  else{
    thermal_traits$dummy_nest_sub[i] <-0
  }
}

#create dummy_excav_occup, Renter=1, Excavator=0
for(i in 1:nrow(thermal_traits)){
  if(thermal_traits$excav_occup[i]=="Renter"){
    thermal_traits$dummy_excav_occup[i] <- 1
  }
  else{
    thermal_traits$dummy_excav_occup[i] <- 0
  }
}

#create dummy_speciality, Oligolectic=0, Polylectic=1
for(i in 1:nrow(thermal_traits)){
  if(thermal_traits$speciality[i]=="Polylectic"){
    thermal_traits$dummy_speciality[i] <- 1
  }
  else{
    thermal_traits$dummy_speciality[i] <- 0
  }
}

```

#This code chunk selects fixed effects for traits mixed model and runs the mixed model
```{r traits analysis}
#StepAIC to determin fixed effects
traits_glm <- glm(ctmax_thermo~ elevation + dummy_sex + mass + it_length + dummy_nest_loc + dummy_nest_sub + dummy_excav_occup + dummy_speciality + phenology, data=thermal_traits)

traits_step <- stepAIC(traits_glm, direction= "both", trace=FALSE)
traits_step$anova

#add site as a random effect to final model
require(lmerTest)
mixed.traits_final <- lmer(ctmax_thermo ~ mass + dummy_speciality + elevation + phenology + (1|site), data=thermal_traits, REML=FALSE)
summary(mixed.traits_final)
```

#This code chunk determines which year to use in the hotter-than-average year analysis
```{r climate}
#make year a factor so you can group_by
climate$year <- as.factor(climate$year)

#add date
climate <- climate %>% mutate(date_sampled = paste(year, month, day, sep="-"), date_sampled_d = as.Date(date_sampled,format='%Y-%B-%d'))
names(climate)

#df with means across entire sampling period
climate_summary <- climate %>% group_by(year) %>% summarize(av_min_temp=mean(temp_min), av_max_temp=mean(temp_max))
climate_summary %>% filter(av_max_temp>mean(climate_summary$av_max_temp))
climate_summary

#df with years included in phenology project
climate_lt <- climate_summary %>% filter(year %in% 2009:2018)
climate_lt %>% filter(av_max_temp>mean(climate_lt$av_max_temp))
```
 
#This code chunk takes the long term bee data stored in df bees_clean and creates a df that describes abundances for resolved species in 2012 and 2013
```{r lt exploration}
#create a dummy date (year=1800) to only select rows within the sampling period
bees_clean <- bees_clean %>% mutate(dummy_date= ymd(paste("1800", month(sample_day), day(sample_day) , sep="-")))

#create df that only includes dates in the sampling interval
int_bees_clean <- bees_clean%>% filter(dummy_date >= as.Date("1800-06-29") & dummy_date <= as.Date("1800-07-25"))

#number of days sampled in interval
int_bees_clean %>% group_by(year) %>% summarize(days_sampled=length(unique(dummy_date)))

#vector with resolved taxa
resolved_taxa <- c("Dufourea harveyi","Halictus rubicundus","Halictus virgatellus","Hoplitis robusta", "Lasioglossum inconditum", "Lasioglossum nigrum", "Lasioglossum ruidosense", "Lasioglossum tenax", "Panurginus cressoniellus", "Panurginus ineptus", "Pseudopanurgus bakeri")

#I am going to start by including on the those specimen we have resolved to species separating net and bowl data
resolved_taxa <- int_bees_clean %>% filter(as.factor(genus_species) %in% resolved_taxa) %>% mutate(year_method = paste(method_clean, year, sep="_")) %>% group_by(genus_species, year_method) %>% summarize(count=n()) 

#spread the df so you can see across methods and years
#this demonstrates that everything you captured in the net you captured in the bowls also so I feel good about only include bowl data
resolved_taxa_summ <-spread(resolved_taxa, key=year_method, value=count)

#add a dummy date to the sampling effort data and filter to only include sampling day
effort <- effort %>% mutate(dummy_date= ymd(paste("1800", month(sample_day), day(sample_day) , sep="-"))) %>% filter(dummy_date >= as.Date("1800-06-29") & dummy_date <= as.Date("1800-07-25"))

#describe the number of hours
effort %>% group_by(as.factor(year)) %>% summarize(tot_bowl_hr = sum(as.duration(hm(bowl_time))))

#define sampling effort
samp_eff_12 <- 484620/(60*60)
samp_eff_13 <- 314640/(60*60)

```

#Add sampling effort to catch in 2012 and 2013 and create climate model
```{r now with sampling effort}
#add a column with the difference between 2012 and 2013 accounting for sampling effort
resolved_taxa_summ <- resolved_taxa_summ %>% mutate(diff_12_13= (bowl_2013/samp_eff_13) - (bowl_2012/samp_eff_12))

#add ctmax and sd of ctmax
resolved_taxa_summ <- left_join(resolved_taxa_summ, thermal_clean %>% group_by(genus_species) %>% summarize(ctmax=mean(ctmax_thermo), sd=sd(ctmax_thermo)), by="genus_species")

#test for influential point
plot(cooks.distance(glm(diff_12_13 ~ctmax, data=resolved_taxa_summ %>% filter() ), family=gaussian))

#without influential point
#tiff('catch_regression.tiff', units="in", width=16, heigh=9, res=300)
ggplot(resolved_taxa_summ %>% filter(genus_species!= "Lasioglossum inconditum"), aes(ctmax, diff_12_13))+
  geom_point(size=4) + 
  geom_errorbarh(aes(xmin=ctmax-sd, xmax=ctmax+sd), width=.2,
                 position=position_dodge(0.05)) + 
  stat_smooth(method="glm", fullrange=TRUE) + 
  theme_classic() +
  labs(x="CTmax", y="Change in Catch Rate") + 
  theme(axis.text = element_text(size=20, color="black"), axis.title=element_text(size=24, color="black"))
#dev.off()

#hotter than average year model, use Gaussian distribution
diff_12_13_glm <- glm(diff_12_13~ctmax, data=resolved_taxa_summ%>% filter(genus_species!= "Lasioglossum inconditum"), family=gaussian)
summary(diff_12_13_glm)

#glm weighted by confidence
diff_12_13_glm_w <- glm(diff_12_13~ctmax, data=resolved_taxa_summ%>% filter(genus_species!= "Lasioglossum inconditum"), family=gaussian, weights=1/sd)
summary(diff_12_13_glm_w)

#McFadden's pseudoR2 1- residual/null
1-(0.110047/0.19223)
```
